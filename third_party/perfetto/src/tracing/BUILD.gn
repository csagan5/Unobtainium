# Copyright (C) 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("../../gn/perfetto.gni")
import("//build_overrides/build.gni")

# Core tracing library, platform independent, no transport layer.
source_set("tracing") {
  public_deps = [
    "../../include/perfetto/tracing/core",
  ]
  deps = [
    "../../gn:default_deps",
    "../../protos:lite",
    "../../protos:zero",
    "../base",
  ]
  sources = [
    "core/chunked_protobuf_input_stream.cc",
    "core/chunked_protobuf_input_stream.h",
    "core/data_source_config.cc",
    "core/data_source_descriptor.cc",
    "core/id_allocator.cc",
    "core/id_allocator.h",
    "core/service_impl.cc",
    "core/service_impl.h",
    "core/shared_memory_abi.cc",
    "core/shared_memory_arbiter_impl.cc",
    "core/shared_memory_arbiter_impl.h",
    "core/trace_config.cc",
    "core/trace_packet.cc",
    "core/trace_writer_impl.cc",
    "core/trace_writer_impl.h",
  ]
}

# Posix specialization of the tracing library for Linux/Android/Mac. Provides
# an IPC transport over a UNIX domain socket.
static_library("ipc") {
  public_deps = [
    "../../include/perfetto/tracing/core",
    "../../include/perfetto/tracing/ipc",
  ]
  sources = [
    "ipc/consumer/consumer_ipc_client_impl.cc",
    "ipc/consumer/consumer_ipc_client_impl.h",
    "ipc/posix_shared_memory.cc",
    "ipc/posix_shared_memory.h",
    "ipc/producer/producer_ipc_client_impl.cc",
    "ipc/producer/producer_ipc_client_impl.h",
    "ipc/service/consumer_ipc_service.cc",
    "ipc/service/consumer_ipc_service.h",
    "ipc/service/producer_ipc_service.cc",
    "ipc/service/producer_ipc_service.h",
    "ipc/service/service_ipc_host_impl.cc",
    "ipc/service/service_ipc_host_impl.h",
  ]
  deps = [
    ":tracing",
    "../../gn:default_deps",
    "../../protos/tracing_service",
    "../base",
    "../ipc",
  ]
}

source_set("tracing_unittests") {
  testonly = true
  deps = [
    ":ipc",
    ":tracing",
    "../../gn:default_deps",
    "../../gn:gtest_deps",
    "../../protos:lite",
    "../../protos:zero",
    "../base",
    "../base:test_support",
  ]
  sources = [
    "core/chunked_protobuf_input_stream_unittest.cc",
    "core/id_allocator_unittest.cc",
    "core/service_impl_unittest.cc",
    "core/shared_memory_abi_unittest.cc",
    "core/shared_memory_arbiter_impl_unittest.cc",
    "core/trace_packet_unittest.cc",
    "core/trace_writer_impl_unittest.cc",
    "ipc/posix_shared_memory_unittest.cc",
    "test/aligned_buffer_test.cc",
    "test/aligned_buffer_test.h",
    "test/test_shared_memory.cc",
    "test/test_shared_memory.h",
    "test/tracing_integration_test.cc",
  ]
}

if (!build_with_chromium) {
  source_set("tracing_benchmarks") {
    testonly = true
    deps = [
      "../../gn:default_deps",
      "//buildtools:benchmark",
    ]
    sources = [
      "test/hello_world_benchmark.cc",
    ]
  }
}
